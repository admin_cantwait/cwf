# About CWF #

CWF is a bundled project which has set of defined framework artifacts. All the commonly shared libraries are to be developed in this project. These developed artifacts are to be added as dependencies for rest of the Applications. 

### What artifacts do we have in this repository? ###

* Date Framework
* Notification Factory
* Print Functionalities
* DB Helper
* Network Connectivity
* Response Helper 
* Exception Framework
* Checkstyle
* Logging
* Caching


### Jenkins CI Server Access ###
URL : http://ec2-52-34-218-111.us-west-2.compute.amazonaws.com:8080

Credentials : admin/admin1@3

All the team members would be notified in case of any version changes. 

***This repository has very limited access. Please contact administrator in case of any additions.***