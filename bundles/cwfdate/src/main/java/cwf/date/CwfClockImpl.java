package cwf.date;

import java.util.Calendar;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

@Component
public class CwfClockImpl implements CwfClock {
	synchronized void init() {
		//initTimeZone();
		//TODO : init some time zones here
	}

	public long millis() {
		return System.currentTimeMillis();
	}

	public TimeZone getTimeZone() {
		// TODO Auto-generated method stub
		return null;
	}

	public Calendar cal() {
		return Calendar.getInstance();
	}
}
