package cwf.date;

import java.util.Calendar;
import java.util.TimeZone;

import org.springframework.stereotype.Component;

/**
 * CwfClock contain predefined methods related to clock
 * @author svalapi
 *
 */
@Component
public interface CwfClock {
	long millis();
	TimeZone getTimeZone();
	Calendar cal();
}
