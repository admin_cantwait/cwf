package cwf.util;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import cwf.date.CwfClock;
import cwf.date.CwfClockImpl;

/**
 * CwfTimeUtil- contains utility methods related to time
 * @author svalapi
 *
 */
@Component
public class CwfTimeUtil {
	CwfClock cwfClock = new CwfClockImpl();
	
	/**
	 * Convert Date object to Calendar
	 * @param date
	 * @return
	 */
	public Calendar toCalendar(Date date) {
		if(date == null) {
			return null;
		}
		Calendar cal = cwfClock.cal();
		cal.setTime(date);
		return cal;
	}
}
