package cwf.dbhelper;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import cwf.dbhelper.sequencegenerator.SequenceDao;

/**
 * @author: yogeshsadula
 */
@Component
public class SenseContext {
	@Autowired SequenceDao sequenceDao;

	public MongoTemplate getSenseDbInstance() {
		// Use below code for Java Annotation based configuration
		/*
		 * ApplicationContext ctx = new
		 * AnnotationConfigApplicationContext(SpringConfig.class);
		 */

		// Below code is used for XML based configuration
		ApplicationContext context = new GenericXmlApplicationContext("spring-config.xml");
		return (MongoTemplate) context.getBean("senseMongoTemplate");
	}

	public long getNextSequenceId(String tableName) {
		return sequenceDao.getNextSequenceId(tableName);
	}

	public DB getDb() {
		MongoCredential credential = MongoCredential.createCredential("admin", "cantwait-database",
				"password".toCharArray());
		MongoClient mongoClient = new MongoClient(new ServerAddress("ds059115.mongolab.com", 59115),
				Arrays.asList(credential));
		return mongoClient.getDB("cantwait-database");
	}
}