package cwf.helper.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class Validation {
	public Boolean isBlankOrEmptyOrNull(String str) {
		return StringUtils.isEmpty(str) || StringUtils.isBlank(str) || str == null ? true : false;
	}
}
